﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kollektsioonid
{
    class Program
    {
        static void Main(string[] args)
        { int[] arvud = { 1, 2, 3, 4, 5, 6, 7 };
            int[] ruudud = new int[10];
            List<int> arvudelist = new List<int>(100) { 1, 2, 3, 4, 5, 6, 7 };//100 TÄHENDAB ET LISTIS ON 100 KOHTA. lISTI EELIS ON SEE, ET SEALT SAAB MUUTA MUUTUJAID.

            arvudelist.Add(14);//LISAB LISTI NUMBRI 14
            arvudelist.Remove(4);//EEMALDAB NUMBRI 4
            arvudelist.RemoveAt(1);//EEMALDAB NUMBRI MIS ON TEISEL KOHAL


            foreach (var x in arvud) Console.WriteLine(x);
            foreach (var x in arvudelist) Console.WriteLine(x);
            Console.WriteLine(arvudelist.Count);// LOEB KOKKU PALJU SEAL MUUTUJAID ON
            Console.WriteLine(arvudelist.Capacity);// LOEB KOKKU PALJU ÜLDSE ON VÕIMALIK NUMBREID SINNA PANNA, SELLE MÄÄRAMISE ÄRA ALGUSES JA PANIME SINNA 100. KUI EI PANEKS SEDA, SIIS OLEKS 8, ehk see mis päriselt on

            SortedSet<int> sorditud = new SortedSet<int> { 1, 7, 2, 8, 3, 1, 9, 2, 14 }; //enne ei saa neid kasutada, kui sa ''new'' operaatoriga neid ei tekita
            Dictionary<string, int> nimekiri = new Dictionary<string, int>();
            // esimene koht on võti, teine koht on väärtus
            nimekiri.Add("Mart", 68);
            nimekiri.Add("Thea", 72);//kahte samasugust muutujat ''Thea'' nt ei saa lisada, tuleb error
            nimekiri["Thea"] = 128;//saad muuta nii

            Dictionary<string, string> nimekiri2 = new Dictionary<string, string>()
            {
                {"love","armastus" },
                 {"love","armastus"},
                 {"love","armastus" },
                 {"love","armastus" },
            };
            Console.WriteLine(nimekiri.Values.Average());



            Console.WriteLine(nimekiri["Thea"]);

                }
    }
}
